FROM openjdk:8

RUN cd "/tmp" && \
    apt-get update && \
    # -----------------------------
    echo "installing basic tools ..." && \
    apt-get -y install gcc-9 && \
    apt-get -y install g++-9 && \
    apt-get -y install make && \
    # -----------------------------
    ln -sf /usr/bin/cpp-9 /usr/bin/cpp && \
    ln -sf /usr/bin/gcc-9 /usr/bin/gcc && \
    ln -sf /usr/bin/gcc /usr/bin/cc && \
    ln -sf "/usr/bin/g++-9" "/usr/bin/g++" && \
    ln -sf /usr/bin/gcc-ar-9 /usr/bin/gcc-ar && \
    ln -sf /usr/bin/gcc-nm-9 /usr/bin/gcc-nm && \
    ln -sf /usr/bin/gcc-ranlib-9 /usr/bin/gcc-ranlib && \
    ln -sf /usr/bin/gcov-9 /usr/bin/gcov && \
    ln -sf /usr/bin/x86_64-linux-gnu-cpp-9 /usr/bin/x86_64-linux-gnu-cpp && \
    ln -sf /usr/bin/x86_64-linux-gnu-gcc-9 /usr/bin/x86_64-linux-gnu-gcc && \
    ln -sf "/usr/bin/x86_64-linux-gnu-g++-9" "/usr/bin/x86_64-linux-gnu-g++" && \
    ln -sf /usr/bin/x86_64-linux-gnu-gcc-ar-9 /usr/bin/x86_64-linux-gnu-gcc-ar && \
    ln -sf /usr/bin/x86_64-linux-gnu-gcc-nm-9 /usr/bin/x86_64-linux-gnu-gcc-nm && \
    ln -sf /usr/bin/x86_64-linux-gnu-gcc-ranlib-9 /usr/bin/x86_64-linux-gnu-gcc-ranlib && \
    ln -sf /usr/bin/x86_64-linux-gnu-gcov-9 /usr/bin/x86_64-linux-gnu-gcov && \
    # -----------------------------
    echo 
